from dataclasses import dataclass
from typing import Any


@dataclass(frozen=True)
class DataFilter:
    column_name: str
    operator: str
    value: Any
