from os.path import join

from yaml import FullLoader, load


def load_yaml(folder: str, name: str):
    file_name = name
    if not file_name.endswith('.yml'):
        file_name += '.yml'

    full_file_name = join(folder, file_name)
    with open(full_file_name) as f:
        loaded = load(f, Loader=FullLoader)
        return loaded
