import inspect
from typing import Type


def get_derived_classes(module, base_class: Type):
    matched = [obj for _, obj in inspect.getmembers(module)
               if inspect.isclass(obj)
               and not inspect.isabstract(obj)
               and base_class in inspect.getmro(obj)]
    return matched
