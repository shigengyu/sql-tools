import sys
from abc import ABC, abstractmethod
from typing import Optional, Type

from sqltools.utils.module_utils import get_derived_classes


class ColumnNameGenerator(ABC):
    GENERATOR_NAME: str

    def __init__(self,
                 prefix: Optional[str] = None,
                 postfix: Optional[str] = None,
                 ignore_sequence: bool = False,
                 **kwargs) -> None:
        self.prefix = prefix
        self.postfix = postfix
        self.ignore_sequence = ignore_sequence

    def generate_single(self, sequence: Optional[int] = None) -> str:

        column_name = self._generate_base_name()
        if self.prefix:
            column_name = self.prefix + column_name
        if self.postfix:
            column_name = column_name + self.postfix

        if self.ignore_sequence:
            return column_name
        else:
            return f'{column_name}_{sequence}'

    @abstractmethod
    def _generate_base_name(self) -> str:
        raise NotImplementedError()


class SimpleColumnNameGenerator(ColumnNameGenerator):
    GENERATOR_NAME = 'simple'

    def __init__(self,
                 *,
                 base_name: str,
                 prefix: Optional[str] = None,
                 postfix: Optional[str] = None,
                 **kwargs) -> None:
        super().__init__(prefix, postfix, **kwargs)
        self.base_name = base_name

    def _generate_base_name(self) -> str:
        return self.base_name


class TypeInferredColumnNameGenerator(ColumnNameGenerator):
    GENERATOR_NAME = 'type_inferred'

    def __init__(self,
                 *,
                 column_type: str,
                 prefix: Optional[str] = None,
                 postfix: Optional[str] = None,
                 ignore_sequence: bool = False,
                 **kwargs) -> None:
        super().__init__(prefix, postfix, ignore_sequence=ignore_sequence, **kwargs)
        self.column_type = column_type

    def _generate_base_name(self) -> str:
        return self.column_type.lower()


def get_column_name_generator(name: Optional[str] = None) -> Optional[Type[ColumnNameGenerator]]:
    if not name:
        return None

    return next(iter([x for x in get_derived_classes(sys.modules[__name__], base_class=ColumnNameGenerator)
                      if getattr(x, 'GENERATOR_NAME', None) == name]))
