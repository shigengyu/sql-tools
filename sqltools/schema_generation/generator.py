from sqlalchemy import MetaData, Table

from sqltools.configs import InsertDataParams
from sqltools.schema_generation.table_names import get_table_name_generator


class TableGenerator(object):

    def __init__(self, *, table_params: InsertDataParams, metadata: MetaData) -> None:
        self.table_params = table_params
        self.metadata = metadata

    def generate_table(self):
        table_name = self._get_table_name()
        columns = self.table_params.create_columns()
        return Table(table_name, self.metadata, *columns)

    def _get_table_name(self) -> str:
        table_name = self.table_params.table_name
        if not table_name:
            table_name_generator_type = get_table_name_generator(self.table_params.table_name_generator)
            table_name = table_name_generator_type(**(self.table_params.table_name_generator_kwargs or {})).generate()
        return table_name

    def reflect_metadata(self) -> str:
        table_name = self._get_table_name()
        # Load the table from database
        self.metadata.reflect(only=[table_name])
        return table_name
