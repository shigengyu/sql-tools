import sys
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Type

from sqltools.utils.module_utils import get_derived_classes


class TableNameGenerator(ABC):
    GENERATOR_NAME: str

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    @abstractmethod
    def generate(self) -> str:
        raise NotImplementedError()


class SimpleTableNameGenerator(TableNameGenerator):
    GENERATOR_NAME = 'simple'

    def __init__(self, *, table_name: str, **kwargs) -> None:
        super().__init__(**kwargs)
        self.table_name = table_name

    def generate(self) -> str:
        return self.table_name


class TimePostfixTableNameGenerator(TableNameGenerator):
    GENERATOR_NAME = 'time_postfix'

    def __init__(self, *, table_name: str, time_format: str, **kwargs) -> None:
        super().__init__(**kwargs)
        self.table_name = table_name
        self.time_format = time_format

    def generate(self) -> str:
        return f'{self.table_name}_{datetime.utcnow().strftime(self.time_format)}'


def get_table_name_generator(name: str) -> Type[TableNameGenerator]:
    return next(iter([x for x in get_derived_classes(sys.modules[__name__], base_class=TableNameGenerator)
                      if getattr(x, 'GENERATOR_NAME', None) == name]))
