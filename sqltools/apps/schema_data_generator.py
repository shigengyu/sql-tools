import operator

import click
from sqlalchemy import MetaData, Table
from sqlalchemy.engine import Engine
from sqlalchemy.orm import create_session, Query

from sqltools.configs.schema_config import load_root_config, InsertDataParams, UpdateDataParams, DataFilter
from sqltools.data_generation.inserter import DataInserter
from sqltools.schema_generation.generator import TableGenerator


@click.command()
@click.option('-c', '--config', required=True, help='name of the config')
@click.option('--generate-table/--no-generate-table', default=False, help='whether or not to generate table')
@click.option('--insert-data/--no-insert-data', default=False, help='whether or not to insert test data')
@click.option('--insert-rows', type=int, default=0, help='number of rows to insert')
def main(config: str,
         generate_table: bool,
         insert_data: bool,
         insert_rows: int):
    root_config = load_root_config(config)

    for action in root_config.actions:
        if action.action_type == 'insert':
            # Create engine
            engine: Engine = root_config.get_connection_config(action.connection_name).create_engine()

            # Create metadata to hold schema information
            metadata = MetaData(bind=engine)

            insert_data_params: InsertDataParams = action
            table_generator = TableGenerator(table_params=insert_data_params,
                                             metadata=metadata)
            if generate_table:
                # Generate table SQLAlchemy object attached to metadata
                table = table_generator.generate_table()
                metadata.create_all()
            else:
                table_name = table_generator.reflect_metadata()
                table = metadata.tables[table_name]

            if insert_data:
                session = create_session(bind=engine)
                DataInserter(table, insert_data_params, rows=insert_rows).insert(session)
        elif action.action_type == 'update':
            # Create engine
            engine: Engine = root_config.get_connection_config(action.connection_name).create_engine()

            # Create metadata to hold schema information
            metadata = MetaData(bind=engine)

            update_data_params: UpdateDataParams = action

            metadata.reflect(only=[update_data_params.table_name])
            table: Table = metadata.tables[update_data_params.table_name]
            session = create_session(bind=engine)

            f: DataFilter
            criteria = []
            for f in update_data_params.filters:
                column = next(iter([c for c in table.columns if c.name == f.column_name]))
                op = getattr(operator, f.operator)
                criteria.append(op(column, f.value))

            result: Query = session.query(table).filter(*criteria)
            result.update(update_data_params.values)


if __name__ == '__main__':
    main()
