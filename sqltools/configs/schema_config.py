import uuid
from dataclasses import dataclass
from os.path import dirname
from typing import Optional, Dict, Any, List, Type, Union

from marshmallow_dataclass import class_schema
from sqlalchemy import Column, Sequence, create_engine
from sqlalchemy.dialects.mysql import types as mysql_types
from sqlalchemy.sql.type_api import TypeEngine
from yaml import YAMLObject

from sqltools.schema_generation.column_names import get_column_name_generator, TypeInferredColumnNameGenerator
from sqltools.utils.yaml_utils import load_yaml


@dataclass
class ConnectionConfig(YAMLObject):
    name: str
    protocol: str
    host: str
    port: int
    database: str
    schema: Optional[str]
    username: str
    password: str
    connect_args: Dict[str, Any]

    def create_engine(self):
        return create_engine(self._get_connection_string(), **self.connect_args)

    def _get_connection_string(self) -> str:
        connection_string = f'{self.protocol}://{self.username}:{self.password}@{self.host}:{self.port}/{self.database}'
        if self.schema:
            connection_string += f'/{self.schema}'

        return connection_string


@dataclass
class ColumnParams(YAMLObject):
    type_name: str
    type_kwargs: Optional[Dict[str, Any]] = None
    column_name: Optional[str] = None
    column_name_generator: Optional[str] = None
    column_name_generator_kwargs: Optional[Dict[str, Any]] = None
    auto_increment: bool = False
    # TODO: increment config does not work, only supports start=1, step=1
    # auto_increment_start: int = 1
    # auto_increment_step: int = 1
    primary_key: bool = False
    data_generator: Optional[str] = None
    data_generator_kwargs: Optional[Dict[str, Any]] = None

    def create_column(self, sequence: int):
        mysql_type: Type[TypeEngine] = getattr(mysql_types, self.type_name)
        column_type: TypeEngine = mysql_type(**(self.type_kwargs or {}))

        column_name = self.column_name
        if not column_name:
            # Use type_inferred column name generator by default.
            generator_name = self.column_name_generator or TypeInferredColumnNameGenerator.GENERATOR_NAME
            column_name_generator_type = get_column_name_generator(generator_name)

            # Always pass in column_type as a keyword argument, as any column name generator could leverage it.
            generator_kwargs = dict(column_type=type(column_type).__name__)
            if self.column_name_generator_kwargs:
                generator_kwargs.update(self.column_name_generator_kwargs)

            generator = column_name_generator_type(**generator_kwargs)
            column_name = generator.generate_single(sequence)

        column_args = []
        column_kwargs = dict(primary_key=self.primary_key)
        if self.auto_increment:
            column_args.append(Sequence(f'seq_{uuid.uuid4()}',
                                        start=1,
                                        increment=1))
        else:
            column_kwargs['autoincrement'] = False

        return Column(column_name, column_type, *column_args, **column_kwargs)


@dataclass
class DataActionParams(YAMLObject):
    action_type: str
    connection_name: str


@dataclass
class InsertDataParams(DataActionParams):
    table_name: Optional[str] = None
    table_name_generator: Optional[str] = None
    table_name_generator_kwargs: Optional[Dict[str, Any]] = None
    columns: List[ColumnParams] = None

    def __getitem__(self, item: str) -> Optional[ColumnParams]:
        return next(iter([c for c in self.columns if c.column_name == item]), None)

    def create_columns(self) -> List[Column]:
        columns: List[Column] = []
        column_sequence = 1

        for column_params in self.columns:
            columns.append(column_params.create_column(column_sequence))
            column_sequence += 1

        return columns


@dataclass
class DataFilter:
    column_name: str
    operator: str
    value: Any


@dataclass
class UpdateDataParams(DataActionParams):
    table_name: str
    filters: List[DataFilter]
    values: Dict[str, Any]


@dataclass
class RootConfig(YAMLObject):
    connections: List[ConnectionConfig]
    actions: List[Union[InsertDataParams, UpdateDataParams]]

    def get_connection_config(self, connection_name: str) -> ConnectionConfig:
        return next(iter([c for c in self.connections if c.name == connection_name]))


def load_root_config(name: str) -> RootConfig:
    loaded = load_yaml(dirname(__file__), name)
    schema = class_schema(RootConfig)
    return schema().load(loaded)
