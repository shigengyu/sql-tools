from dataclasses import dataclass
from typing import Generator

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'BitValueDataGenerator',
]


@dataclass
class BitValueDataGenerator(ColumnDataGenerator):

    def generate(self, count: int) -> Generator[bool, None, None]:
        for i in range(count):
            yield i % 2 == 0
