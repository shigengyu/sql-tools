import uuid
from abc import abstractmethod
from dataclasses import dataclass
from typing import Generator, Optional

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'ConstantStringColumnDataGenerator',
    'LengthRangeStringDataGenerator',
    'UUIDStringColumnDataGenerator',
]


class StringColumnDataGenerator(ColumnDataGenerator):

    @abstractmethod
    def generate(self, count: int) -> Generator[str, None, None]:
        raise NotImplementedError()


@dataclass
class ConstantStringColumnDataGenerator(StringColumnDataGenerator):
    value: str

    def generate(self, count: int) -> Generator[str, None, None]:
        for _ in range(count):
            yield self.value


@dataclass
class UUIDStringColumnDataGenerator(StringColumnDataGenerator):

    def generate(self, count: int) -> Generator[str, None, None]:
        for _ in range(count):
            yield str(uuid.uuid4())


@dataclass
class LengthRangeStringDataGenerator(StringColumnDataGenerator):
    length: int
    min_length: int = 0
    base_string: Optional[str] = None

    def generate(self, count: int) -> Generator[str, None, None]:
        # Generate default base string if not provided
        self.base_string = self.base_string or str(uuid.uuid4()).replace('-', '')

        length_values = list(range(self.min_length, self.length + 1))
        for i in range(count):
            index = i % (self.length - self.min_length + 1)
            if index % 2 == 0:
                length = length_values[index // 2]
            else:
                length = length_values[-index // 2]

            base_string = self.base_string
            if length > len(base_string):
                base_string = base_string * ((length // len(base_string)) + 1)

            yield base_string[:length]
