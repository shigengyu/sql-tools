from abc import abstractmethod, ABC
from dataclasses import dataclass
from typing import Generator


@dataclass
class ColumnDataGenerator(ABC):
    def __init__(self, **kwargs):
        super(**kwargs)

    @abstractmethod
    def generate(self, count: int) -> Generator[object, None, None]:
        raise NotImplementedError()
