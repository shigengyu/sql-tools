from dataclasses import dataclass
from random import Random
from typing import Generator

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'RangeDecimalDataGenerator',
]


@dataclass
class RangeDecimalDataGenerator(ColumnDataGenerator):
    precision: int
    scale: int

    def generate(self, count: int) -> Generator[float, None, None]:
        max_value = pow(10, self.precision) - 1

        if count > 0:
            yield max_value
            count -= 1

        if count > 0:
            yield -max_value
            count -= 1

        if count > 0:
            r = Random()
            for _ in range(count):
                precision = r.randint(2, self.precision)
                scale = r.randint(1, self.scale)
                yield r.randint(pow(10, precision - 1), pow(10, precision) - 1) / scale
