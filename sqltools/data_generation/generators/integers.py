from abc import abstractmethod
from dataclasses import dataclass
from random import Random
from typing import Generator

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'ConstantIntDataGenerator',
    'IncrementalIntDataGenerator',
    'RangeIntDataGenerator',
]


@dataclass
class IntDataGenerator(ColumnDataGenerator):

    @abstractmethod
    def generate(self, count: int) -> Generator[int, None, None]:
        raise NotImplementedError()


@dataclass
class ConstantIntDataGenerator(IntDataGenerator):
    value: int

    def generate(self, count: int) -> Generator[int, None, None]:
        for _ in range(count):
            yield self.value


@dataclass
class IncrementalIntDataGenerator(IntDataGenerator):
    start: int = 1
    step: int = 1

    def generate(self, count: int) -> Generator[int, None, None]:
        yield from range(self.start, self.start + self.step * count, self.step)


@dataclass
class RangeIntDataGenerator(IntDataGenerator):
    min_value: int
    max_value: int

    def generate(self, count: int) -> Generator[int, None, None]:
        r = Random()
        for i in range(count):
            if i == 0:
                yield self.min_value
            elif i == 1:
                yield self.max_value
            elif i == 2 and self.min_value < 0 < self.max_value:
                yield 0
            else:
                yield r.randint(self.min_value, self.max_value)
