from dataclasses import dataclass
from datetime import datetime
from typing import Generator, Optional

from dateutil.relativedelta import relativedelta

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'DateTimeDataGenerator',
    'DateDataGenerator',
    'TimeDataGenerator',
]


@dataclass
class DateTimeDataGenerator(ColumnDataGenerator):
    interval_days: int = 0
    interval_hours: int = 0
    interval_minutes: int = 0
    interval_seconds: int = 0
    interval_microseconds: int = 0
    start_value: Optional[str] = None
    start_value_format: Optional[str] = None

    def generate(self, count: int) -> Generator[datetime, None, None]:
        current_time = self.initial_value
        for i in range(count):
            yield current_time
            current_time += self.interval

    @property
    def initial_value(self) -> datetime:
        if self.start_value and self.start_value_format:
            return datetime.strptime(self.start_value, self.start_value_format)
        else:
            return datetime.utcnow()

    @property
    def interval(self) -> relativedelta:
        return relativedelta(days=self.interval_days,
                             hours=self.interval_hours,
                             minutes=self.interval_minutes,
                             seconds=self.interval_seconds,
                             microsecond=self.interval_microseconds)


@dataclass
class DateDataGenerator(DateTimeDataGenerator):

    @property
    def initial_value(self):
        return super().initial_value.replace(hour=0, minute=0, second=0, microsecond=0)


@dataclass
class TimeDataGenerator(DateTimeDataGenerator):

    @property
    def initial_value(self):
        return super().initial_value.time().replace(microsecond=0)
