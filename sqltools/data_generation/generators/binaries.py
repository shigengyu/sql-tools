import uuid
from abc import abstractmethod
from dataclasses import dataclass
from random import Random
from typing import Generator

import six

from sqltools.data_generation.generators.base import ColumnDataGenerator

__all__ = [
    'BinaryColumnDataGenerator',
    'ConstantBinaryColumnDataGenerator',
    'LengthRangeBinaryDataGenerator',
]


class BinaryColumnDataGenerator(ColumnDataGenerator):

    @abstractmethod
    def generate(self, count: int) -> Generator[bytes, None, None]:
        raise NotImplementedError()


@dataclass
class ConstantBinaryColumnDataGenerator(BinaryColumnDataGenerator):
    value: bytes

    def generate(self, count: int) -> Generator[bytes, None, None]:
        for _ in range(count):
            yield self.value


@dataclass
class LengthRangeBinaryDataGenerator(BinaryColumnDataGenerator):
    max_length: int

    def generate(self, count: int) -> Generator[bytes, None, None]:
        base_blob = six.ensure_binary(str(uuid.uuid4()) + '\0')
        blob = base_blob * ((self.max_length // len(base_blob)) + 1)

        if count > 0:
            yield blob[:self.max_length]
            count -= 1

        if count > 0:
            yield bytes()
            count -= 1

        r = Random()
        for _ in range(count):
            yield blob[:r.randint(1, self.max_length)]
