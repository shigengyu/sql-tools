import sys
from dataclasses import dataclass
from typing import Type, Dict, Any, Optional

from sqlalchemy import Column

from sqltools.data_generation.generators import *
from sqltools.data_generation.generators.base import ColumnDataGenerator
from sqltools.utils.module_utils import get_derived_classes


@dataclass
class DataGeneratorDefaultParams(object):
    generator_type: Type[ColumnDataGenerator]
    passthrough_args: Dict[str, str] = None
    default_args: Dict[str, Any] = None


# https://dev.mysql.com/doc/refman/8.0/en/integer-types.html
INTEGER_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'TINYINT': DataGeneratorDefaultParams(RangeIntDataGenerator,
                                          default_args=dict(min_value=-128, max_value=127)),
    'SMALLINT': DataGeneratorDefaultParams(RangeIntDataGenerator,
                                           default_args=dict(min_value=-32768, max_value=32767)),
    'MEDIUMINT': DataGeneratorDefaultParams(RangeIntDataGenerator,
                                            default_args=dict(min_value=-8388608, max_value=8388607)),
    'INTEGER': DataGeneratorDefaultParams(RangeIntDataGenerator,
                                          default_args=dict(min_value=-2147483648, max_value=2147483647)),
    'BIGINT': DataGeneratorDefaultParams(RangeIntDataGenerator,
                                         default_args=dict(min_value=-2e63, max_value=2e63 - 1)),
}

# https://dev.mysql.com/doc/refman/8.0/en/fixed-point-types.html
FIXED_POINT_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'DECIMAL': DataGeneratorDefaultParams(RangeDecimalDataGenerator,
                                          passthrough_args=dict(precision='precision', scale='scale')),
}

# https://dev.mysql.com/doc/refman/8.0/en/floating-point-types.html
FLOATING_POINT_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'FLOAT': DataGeneratorDefaultParams(RangeDecimalDataGenerator,
                                        default_args=dict(precision=23, scale=8)),
    'DOUBLE': DataGeneratorDefaultParams(RangeDecimalDataGenerator,
                                         default_args=dict(precision=53, scale=8)),
    'REAL': DataGeneratorDefaultParams(RangeDecimalDataGenerator,
                                       default_args=dict(precision=53, scale=8)),
}
# https://dev.mysql.com/doc/refman/8.0/en/bit-type.html
BIT_VALUE_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'BIT': DataGeneratorDefaultParams(BitValueDataGenerator),
}

# https://dev.mysql.com/doc/refman/8.0/en/date-and-time-type-syntax.html
DATE_TIME_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    # 'DATE': DataGeneratorDefaultParams(DateDataGenerator,
    #                                    default_args=dict(interval_days=1)),
    'DATETIME': DataGeneratorDefaultParams(DateTimeDataGenerator,
                                           default_args=dict(interval_minutes=1)),
    'TIMESTAMP': DataGeneratorDefaultParams(DateTimeDataGenerator,
                                            default_args=dict(interval_minutes=1)),
    'TIME': DataGeneratorDefaultParams(TimeDataGenerator,
                                       default_args=dict(interval_minutes=1)),
}

# https://dev.mysql.com/doc/refman/8.0/en/char.html
CHAR_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'CHAR': DataGeneratorDefaultParams(LengthRangeStringDataGenerator,
                                       passthrough_args=dict(length='length')),
    'VARCHAR': DataGeneratorDefaultParams(LengthRangeStringDataGenerator,
                                          passthrough_args=dict(length='length')),
}

# https://dev.mysql.com/doc/refman/8.0/en/binary-varbinary.html
BINARY_TYPES: Dict[str, DataGeneratorDefaultParams] = {
    'TINYBLOB': DataGeneratorDefaultParams(LengthRangeBinaryDataGenerator,
                                           default_args=dict(max_length=pow(2, 8))),
    'MEDIUMBLOB': DataGeneratorDefaultParams(LengthRangeBinaryDataGenerator,
                                             default_args=dict(max_length=pow(2, 9))),  # Maximum is pow(2, 24)
    'LONGBLOB': DataGeneratorDefaultParams(LengthRangeBinaryDataGenerator,
                                           default_args=dict(max_length=pow(2, 10))),  # Maximum is pow(2, 32)
}

ALL_TYPES: Dict[str, DataGeneratorDefaultParams] = {}
ALL_TYPES.update(INTEGER_TYPES)
ALL_TYPES.update(FIXED_POINT_TYPES)
ALL_TYPES.update(FLOATING_POINT_TYPES)
ALL_TYPES.update(BIT_VALUE_TYPES)
ALL_TYPES.update(DATE_TIME_TYPES)
ALL_TYPES.update(CHAR_TYPES)
ALL_TYPES.update(BINARY_TYPES)


def get_data_generator(name: Optional[str] = None) -> Optional[Type[ColumnDataGenerator]]:
    if not name:
        return None

    return next(iter([x for x in get_derived_classes(sys.modules[__name__], base_class=ColumnDataGenerator)
                      if x.__name__ == name]))


def get_default_data_generator(column: Column) -> ColumnDataGenerator:
    params = ALL_TYPES[type(column.type).__name__]
    kwargs = {}
    if params.default_args:
        kwargs.update(params.default_args)
    if params.passthrough_args:
        for k, v in params.passthrough_args.items():
            kwargs[k] = getattr(column.type, v)
    return params.generator_type(**kwargs)
