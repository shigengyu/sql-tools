from collections import OrderedDict
from typing import Dict, Generator, Any

from sqlalchemy import Column
from sqlalchemy.orm import Session
from sqlalchemy.testing.schema import Table

from sqltools.configs import InsertDataParams
from sqltools.data_generation.default_generators import get_default_data_generator, get_data_generator
from sqltools.data_generation.generators.base import ColumnDataGenerator


class DataInserter(object):

    def __init__(self, table: Table, table_params: InsertDataParams, rows: int) -> None:
        self.table = table
        self.table_params = table_params
        self.rows = rows

    def generate_data(self) -> Generator[Dict[str, Any], None, None]:
        data_generators: Dict[str, ColumnDataGenerator] = OrderedDict()
        column: Column
        for column in self.table.columns:
            # Do not generate values for identity columns
            if column.autoincrement:
                continue

            column_param = self.table_params[column.name]
            if not column_param or not column_param.data_generator:
                data_generator = get_default_data_generator(column)
            else:
                data_generator_type = get_data_generator(column_param.data_generator)
                if column_param.data_generator_kwargs:
                    data_generator = data_generator_type(**column_param.data_generator_kwargs)
                else:
                    data_generator = data_generator_type()

            data_generators[column.name] = data_generator

        value_generators: Dict[str, Generator[Dict[str, Any], None, None]] = {
            k: v.generate(count=self.rows) for k, v in data_generators.items()
        }

        for i in range(self.rows):
            row_data = {}
            for column_name, g in value_generators.items():
                row_data[column_name] = next(g)

            yield row_data

    def insert(self, session: Session):
        for data in self.generate_data():
            session.execute(self.table.insert(), data)


if __name__ == '__main__':
    pass
