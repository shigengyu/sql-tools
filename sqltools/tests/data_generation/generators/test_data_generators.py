from unittest import TestCase

from sqltools.configs.schema_config import load_root_config
from sqltools.data_generation.default_generators import get_default_data_generator, get_data_generator
from sqltools.data_generation.generators import RangeIntDataGenerator


class TestColumnDataGenerator(TestCase):

    def test_generate_count(self):
        table_params = load_root_config('all_types')
        columns = table_params.create_columns()
        generators = [get_default_data_generator(c) for c in columns]
        count = 100
        generated_count = [len(list(g.generate(count=count))) for g in generators]
        self.assertTrue(all(x == count for x in generated_count))

    def test_get_data_generator(self):
        self.assertTrue(RangeIntDataGenerator, get_data_generator('RangeIntDataGenerator'))
