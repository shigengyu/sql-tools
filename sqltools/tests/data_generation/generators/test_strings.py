from unittest import TestCase

from sqltools.data_generation.generators.strings import LengthRangeStringDataGenerator


class TestLengthRangeStringColumnGenerator(TestCase):

    def test_generate(self):
        max_length = 32
        for x in LengthRangeStringDataGenerator(length=max_length,
                                                min_length=0).generate(count=100):
            self.assertTrue(0 <= len(x) <= 32)
