from unittest import TestCase

from sqltools.data_generation.generators.integers import RangeIntDataGenerator


class TestRangeIntDataGenerator(TestCase):

    def test_generate(self):
        min_value = -100
        max_value = 100
        generated = list(RangeIntDataGenerator(min_value=min_value, max_value=max_value).generate(count=10))
        self.assertEqual(min_value, generated[0])
        self.assertEqual(max_value, generated[1])
        self.assertEqual(0, generated[2])
        for x in generated[3:]:
            self.assertTrue(min_value <= x <= max_value)
