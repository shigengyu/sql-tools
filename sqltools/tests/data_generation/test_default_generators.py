from unittest import TestCase

from sqlalchemy import Column
from sqlalchemy.dialects import mysql

from sqltools.data_generation.default_generators import get_default_data_generator
from sqltools.data_generation.generators.integers import RangeIntDataGenerator
from sqltools.data_generation.generators.strings import LengthRangeStringDataGenerator


class TestDefaultGenerators(TestCase):

    def test_get_default_data_generator_integer(self):
        generator = get_default_data_generator(Column('', mysql.INTEGER))
        self.assertIsInstance(generator, RangeIntDataGenerator)

    def test_get_default_data_generator_varchar(self):
        generator = get_default_data_generator(Column('', mysql.VARCHAR(length=32)))
        self.assertIsInstance(generator, LengthRangeStringDataGenerator)
        self.assertEqual(32, generator.length)
