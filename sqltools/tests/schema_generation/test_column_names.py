from unittest import TestCase

from sqlalchemy.dialects import mysql

from sqltools.schema_generation.column_names import TypeInferredColumnNameGenerator, get_column_name_generator, \
    SimpleColumnNameGenerator


class TestTypeInferredColumnNameGenerator(TestCase):

    def setUp(self) -> None:
        self.generator = TypeInferredColumnNameGenerator(column_type=mysql.INTEGER.__name__)

    def test_generate_multiple_default_start(self):
        generated = self.generator.generate_multiple(count=10)
        self.assertEqual('integer_1', generated[0])
        self.assertEqual('integer_10', generated[-1])

    def test_generate_multiple_specific_start(self):
        generated = self.generator.generate_multiple(count=10, start=5)
        self.assertEqual('integer_5', generated[0])
        self.assertEqual('integer_14', generated[-1])


class TestColumnNames(TestCase):

    def test_get_column_name_generator(self):
        self.assertEqual(SimpleColumnNameGenerator, get_column_name_generator('simple'))
