import uuid
from operator import gt

from sqlalchemy import Column, String, create_engine, MetaData, Table
from sqlalchemy import Sequence
from sqlalchemy.dialects import mysql
from sqlalchemy.orm import create_session
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class User1(Base):
    __tablename__ = 'users_1'

    user_id = Column(mysql.TINYINT, Sequence(f'seq_{uuid.uuid4()}'), primary_key=True)
    name = Column(String(32))
    fullname = Column(String(32))
    nickname = Column(String(32))

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (self.name, self.fullname, self.nickname)


if __name__ == '__main__':
    username = 'admin'
    password = 'NXGgwkPfuWvyhkk8jYA2nMrb8zEqbsje'
    hostname = 'mysql-dev.tychontrading.com'
    database = 'tychon_dev'
    engine = create_engine(f'mysql+pymysql://{username}:{password}@{hostname}/{database}', encoding='latin1', echo=True)

    metadata = MetaData(bind=engine)
    t = Table('users_2', metadata,
              Column('user_id', mysql.TINYINT, Sequence(f'seq_{uuid.uuid4()}'), primary_key=True),
              Column('name', String(32)),
              Column('fullname', String(32)),
              Column('nickname', String(32)))
    metadata.create_all(bind=engine)

    session = create_session(bind=engine, autocommit=False, autoflush=False)

    user = User1(name='Gengyu',
                 fullname='Gengyu Shi',
                 nickname='dummy')

    session.add(user)
    session.commit()

    users = session.query(User1).filter(gt(User1.user_id, 1))
    for user in users:
        user.nickname += 'Updated'
    session.commit()

    session.close()
