# SQL Tools

## Introduction

**sql-tools** is a Python package that allows users mockup database actions.
It includes but does not limit to`INSERT`, `UPDATE`, `DELETE` actions,  which are *yaml* configuration driven.
Users are able to define flexible plug-in action executors to handle all kinds of data / schema scenarios.
It uses `sqlalchemy` which allows connecting to different types of database seamlessly.

## Main Features
All **sql-tools** features are driven by **yaml** format configuration.
**yaml** configuration contains:
1. Database connection properties
1. Actions - one of the following types
    1. `insert` - Create a table and insert test data
    1. `update` - Update rows in a table with specific query criteria and new values
    1. `delete` - Delete rows in a table with specific query criteria

### Schema Generation
**sql-tools** support flexible schema generation which allows you to specify:
1. Fixed or rule generated table name
1. Fixed or rule generated  column names
1. Pre-defined column types

A sample **yaml** configuration looks like this:
```yaml
actions:
  - action_type: insert
    connection_name: sql_test
    table_name_generator: time_postfix
    table_name_generator_kwargs:
      table_name: all_types
      time_format: '%Y%m%d%H%M%S'
    columns:
      - column_name: id
        type_name: INTEGER
        auto_increment: true
        primary_key: true
      - type_name: INTEGER
      - type_name: DECIMAL
        type_kwargs:
          precision: 20
          scale: 8
      - type_name: FLOAT
      - type_name: DOUBLE
      - type_name: VARCHAR
        type_kwargs:
          length: 64
```

#### Insert Action Configuration

| Property                    | Required? | Comments |
| --------------------------- | --------- | -------- |
| action_type                 | Required  | Must be set to `insert`
| connection_type             | Required  | References a defined connection
| table_name                  | Optional  | Fixed table name
| table_name_generator        | Optional  | Table name generator if fixed table name not specified
| table_name_generator_kwargs | Optional  | Optional `kwargs` passed into table name generator
| columns                     | Required  | List of columns

`columns` configuration contains following attributes:

| Property                     | Required? | Comments |
| ---------------------------- | --------- | -------- |
| type_name                    | Required  | Type of the column
| type_kwargs                  | Optional  | Optional `kwargs` passed through into `sqlalchemy` column type
| column_name                  | Optional  | Fixed column name
| column_name_generator        | Optional  | Column name generator if fixed column name not specified
| column_name_generator_kwargs | Optional  | Optional `kwargs` passed into column name generator
| auto_increment               | Optional  | `true` if column is identity, otherwise `false`
| primary_key                  | Optional  | `true` if column is primary key, otherwise `false`
| data_generator               | Optional  | Data generator used to generate test data for the column
| data_generator_kwargs        | Optional  | Optional `kwargs` passed into data generator

* Column name rules:
   1. Use `column_name` if specified
   1. Use `column_name_generator` and `column_name_generator_kwargs` to instantiate column name generator to generate column name
   1. Infer column name from column type

### Mock Data Insert

### Mock Data Update

### Mock Data Delete
